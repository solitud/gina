<?php
App::uses('Libterm', 'Gina.Model');

/**
 * Libterm Test Case
 *
 */
class LibtermTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.gina.libterm'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Libterm = ClassRegistry::init('Gina.Libterm');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Libterm);

		parent::tearDown();
	}

}
