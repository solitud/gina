#Gina
CakePHP plugin for test data generation

##What it does
Gina is capable to analyze your database relations and create relational data with valid keys.  
Gina will parse database fields and tries to guess the best type of data for the fields, e.g. datetime strings for datetime fields and multi-paragraph text for text fields.  
Gina creates a config file which you can manually adjust to your needs.

##How to install
1. Drop/clone Gina to your app/Plugin folder.
2. Open your general bootstrap file in /app/Config/bootstrap.php
3. Add this line:

	CakePlugin::load('Gina');

##How to use
1. Open your terminal and cd in your app directory.
2. From there type

	./Console/cake Gina.gina

3. Initialize the Gina test data table by pressing *[C]reate gina SQL table*
4. Execute Gina again then analyze your database by pressing *[A]nalyse Database and generate Gina Config File*
5. Open and edit the newly generated file in

	/app/Plugin/Gina/Config/gina_config.php

6. Execute Gina again and generate test data by pressing *[G]enerate Test Data*

##Requirements
* compatible with CakePHP > 2.3
* PHP CLI

***
Knut Schade | knut@solitud.de  
Releases under MIT License
