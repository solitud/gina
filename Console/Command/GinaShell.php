<?php
/*
Gina Shell - CakePHP Testdata generator
Knut Schade 2014
 */

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

// Run ./Console/cake Gina.gina
class GinaShell extends AppShell {

    public $targettedPlugin = false;
    public $pluginPrefix = false;

    public $uses = array('Gina.Libterm');
    public $data = array();
    public $idList = array();
    public $dataScheme = array();
    public $configFile;
    public $quantityOfDataEntriesForModel = array();
    public $quantityOfEntriesinRelatedLists = 5000;

    public $defaults = array(
        'from' => '2011-01-01',
        'to' => '2014-01-01',
    );

    public function initialize() {
        parent::initialize();
        $this->stdout->styles('bold', array('text' => 'blue', 'bold' => true));
        $this->stdout->styles('cn', array('text' => 'cyan'));

        $this->pluginPrefix = $this->targettedPlugin ? $this->targettedPlugin . '.' : '';
    }

    public function main() {
        $this->out($this->nl());
        $this->out('<bold>The Gina Shell</bold>');
        $this->hr();

        $this->out('[C]reate gina SQL table');
        $this->out('[A]nalyse Database and generate Gina Config File');
        $this->out('[G]enerate Test Data');

        $this->out($this->nl());

        $selection = $this->in('Select a task:', array('C', 'A', 'G'), 'A');
        $selection = strtoupper($selection);

        switch ($selection) {
            case 'C':
                $this->generateSQLTable();
                break;

            case 'A':
                $this->generateTestSchema();
                break;

            case 'G':
                $this->generateTestData();
                break;
        }
        $this->out('<info>finished</info>');
    }

    public function generateTestSchema() {
        if ($this->targettedPlugin) {
            $models = App::objects($this->pluginPrefix . 'Model');
        } else {
            $models = App::objects('model');
        }

        if(!empty($models)) {
            $this->configFile = new File(App::pluginPath('Gina') . "Config" . DS . 'gina_config.php', true, 0644);
            $this->configFile->write("<?php\n", 'w', true);

            $this->addDocBlock();

            foreach ($models as $key => $modelName) {
                $this->generateTestSchemaForModel($modelName);
            }
            $this->out("config file generated in " . App::pluginPath('Gina') . "Config" . DS . "gina_config.php");
        } else {
            $this->out('no models found');
        }
    }

    public function getDataScheme() {
        Configure::load('Gina.gina_config');
        $this->quantityOfDataEntriesForModel = Configure::read('QuantityOfData');
        $this->dataScheme = Configure::read('Data');
    }

    public function generateTestSchemaForModel($modelName) {
        try {

            $model = ClassRegistry::init($this->pluginPrefix . $modelName);

            $schema = $model->schema();
            unset($schema['id']);

            $configEntry = '$config["QuantityOfData"]["' . $modelName . '"] = 10;';
            $this->out($configEntry);
            $this->configFile->append($configEntry . "\n", true);

            foreach ($schema as $key => $params) {
                switch($params['type']) {
                    case 'integer' :
                        $keyParts = explode('_', $key);
                        if(array_pop($keyParts) == 'id') {
                            $dataFunc = array('type' => 'primaryKey', 'model' => ucfirst(implode('_', $keyParts)));
                        } else {
                            $dataFunc = array('type' => 'integer', 'from' => 0, 'to' => 10);
                        }
                        break;
                    case 'datetime' :
                        if(isset($schema['created']) && $key == 'modified') {
                            $dataFunc = array('type' => 'datetime', 'from' => $this->defaults['from'], 'to' => $this->defaults['to'], 'after' => 'created');
                        } else {
                            $dataFunc = array('type' => 'datetime', 'from' => $this->defaults['from'], 'to' => $this->defaults['to']);
                        }
                        break;
                    case 'string' :
                        $dataFunc = array('type' => 'name');
                        break;
                    case 'text' :
                        $dataFunc = array('type' => 'text', 'paragraphs' => 2, 'book' => 'journey_in_other_worlds.txt');
                        break;
                    case 'boolean' :
                        $dataFunc = array('type' => 'boolean');
                        break;
                    default:
                        $dataFunc = array('type' => null);
                }

                $configEntry = '$config["Data"]["' . $modelName . '"]["' . $key . '"] = ' . $this->var_export_min($dataFunc, true) . ';';
                $this->out($configEntry);
                $this->configFile->append($configEntry . "\n", true);

            }
            $this->configFile->append("\n", true);
            $this->out($this->nl());
        } catch(Exception $e) {
        }
    }

    public function generateTestData() {
        $this->setListOfIdsForRelatedModels();
        $this->getDataScheme();

        foreach ($this->dataScheme as $modelName => $fields) {
            $model = ClassRegistry::init($this->pluginPrefix . $modelName);

            for($i=0; $i < $this->quantityOfDataEntriesForModel[$modelName]; $i++) {
                foreach ($fields as $fieldKey => $fieldParams) {
                    $this->data[$fieldKey] = $this->generateFieldData($fieldKey, $fieldParams);
                }
                $data[$modelName] = $this->data;
                // debug($data);
                $model->create();
                $model->save($data, false);
                $data = $this->data = array();
            }
            $this->idList[$modelName] = $model->find('list', array(
                'limit' => $this->quantityOfEntriesinRelatedLists
            ));
        }
    }

    public function generateFieldData($fieldKey, $params) {
        if(!is_array($params)) {
            return $params;
        }
        switch($params['type']) {
            case 'primaryKey' :
                $value = isset($this->idList[$params['model']])
                    ? array_rand($this->idList[$params['model']])
                    : null;
                break;
            case 'datetime' :
                $from = isset($params['from']) ? $params['from'] : $this->defaults['from'];
                $to = isset($params['to']) ? $params['to'] : $this->defaults['to'];
                $precision = isset($params['precision']) ? $params['precision'] : 'hour';
                $maxTimeSpan = isset($params['maxTimeSpan']) ? $params['maxTimeSpan'] : null;

                if(isset($params['after']) && isset($this->data[$params['after']])) {
                    $from = $this->data[$params['after']];
                }

                $value = $this->Libterm->getRandomDatetime(array('startTime' => $from, 'endTime' => $to, 'precision' => $precision, 'maxTimeSpan' => $maxTimeSpan));

                break;
            case 'name' :
                $value = $this->Libterm->getRandomname();
                break;
            case 'firstname' :
                $value = $this->Libterm->getFirstname();
                break;
            case 'lastname' :
                $value = $this->Libterm->getLastname();
                break;
            case 'company' :
                $value = $this->Libterm->getCompanyname();
                break;
            case 'job' :
                $value = $this->Libterm->getJobtitle();
                break;
            case 'text' :
                $value = $this->Libterm->getRandomtext($params['paragraphs'], $params['book']);
                break;
            case 'boolean' :
                $value = rand(0,1);
                break;
            case 'integer' :
                $params['from'] = isset($params['from']) ? $params['from'] : 0;
                $params['to'] = isset($params['to']) ? $params['to'] : 10000;
                $value = rand($params['from'], $params['to']);
                break;
            default:
                $value = null;
        }
        return $value;
    }

    public function setListOfIdsForRelatedModels() {
        $models = App::objects('model');
        foreach ($models as $modelName) {
            try {
                $this->idList[$modelName] = ClassRegistry::init($modelName)->find('list', array(
                    'limit' => $this->quantityOfEntriesinRelatedLists
                ));
            } catch(Exception $e) {

            }
        }

        if ($this->targettedPlugin) {
            $models = App::objects($this->targettedPlugin . '.Model');
            foreach ($models as $modelName) {
                try {
                    $this->idList[$modelName] = ClassRegistry::init($this->pluginPrefix . $modelName)->find('list', array(
                        'limit' => $this->quantityOfEntriesinRelatedLists
                    ));
                } catch(Exception $e) {

                }
            }
        }
    }

    public function generateSQLTable () {
        $sqlDump = file_get_contents(App::pluginPath('Gina') . "Config" . DS . 'Schema' . DS .  'schema.sql', 'FILE_TEXT');
        $this->Libterm->useTable = false;
        $this->Libterm->query($sqlDump);
    }

    public function var_export_min($var, $return = false) {
        if (is_array($var)) {
            $toImplode = array();
            foreach ($var as $key => $value) {
                $toImplode[] = var_export($key, true).'=>'.$this->var_export_min($value, true);
            }
            $code = 'array('.implode(',', $toImplode).')';
            if ($return) return $code;
            else echo $code;
        } else {
            return var_export($var, $return);
        }
    }

    public function addDocBlock() {
        $docBlock = file_get_contents(App::pluginPath('Gina') . "Config" . DS . 'docblock.md');
        $this->configFile->append("/*\n", true);
        $this->configFile->append($docBlock . "\n", true);
        $this->configFile->append("*/\n\n", true);
    }

/**
 * Gets Photos from Flickr Feed. NO Api key required.
 * @return [type] [description]
 */
    public function getImagesFromFlickrFeed() {
        $tags = array('wave', 'girls', 'punk', 'emo', 'rhino');
        foreach ($tags as $tag) {
            $data = unserialize(file_get_contents('http://api.flickr.com/services/feeds/photos_public.gne?format=php_serial&tags=' . $tag));
            foreach ($data['items'] as $key => $item) {
                $this->copyImg($item['photo_url'], md5($item['url']). '.jpg');
            }
        }
    }

/**
 * Gets Photos from Flickr API. Api key required.
 * @return [type] [description]
 */
    public function getImagesFromFlickrApi() {
        $apiKey = 'xyz';
        $jsonStr = file_get_contents('https://api.flickr.com/services/rest/?method=flickr.photos.search&extras=url_l&per_page=500&api_key=' . $apiKey . '&format=json&nojsoncallback=1&content_type=1&media=photos&sort=interestingness-desc&text=emo');
        $data = json_decode($jsonStr, true);
        foreach ($data['photos']['photo'] as $key => $p) {
            if(isset($p['url_l'])) {
               $this->copyImg($p['url_l'], Inflector::slug($p['title']) . '.jpg');
            }
        }
    }

/**
 * Gets Photos from 4Chan API. NO Api key required.
 * @return [type] [description]
 */
    public function getImagesFrom4ChanCatalog() {
        $board = 'p';
        $jsonStr = file_get_contents('http://a.4cdn.org/' . $board . '/catalog.json');
        $json = json_decode($jsonStr, true);

        foreach ($json as $page) {
            foreach ($page['threads'] as $thread) {
                if(isset($thread['filename'])) {
                    $imgUrl = 'http://i.4cdn.org/' . $board . '/' . $thread['tim'] . $thread['ext'];
                    $this->copyImg($imgUrl, Inflector::slug($thread['no'] . '_' . $thread['filename']) . $thread['ext']);
                }
                if(isset($thread['last_replies'])) {
                    foreach ($thread['last_replies'] as $reply) {
                        if(isset($reply['filename'])) {
                            $imgUrl = 'http://i.4cdn.org/' . $board . '/' . $reply['tim'] . $reply['ext'];
                            $this->copyImg($imgUrl, Inflector::slug($reply['no'] . '_' . $reply['filename']) . $reply['ext']);
                        }
                    }
                }
            }
        }

    }

/**
 * Gets Photos from 4Chan API. NO Api key required.
 * @return [type] [description]
 */
    public function getImagesFrom4ChanThread() {
        $board = 'p';
        $threadId = '12345';
        $jsonStr = file_get_contents('http://a.4cdn.org/' . $board . '/thread/' . $threadId . '.json');
        $json = json_decode($jsonStr, true);

        foreach ($json['posts'] as $post) {
            if(isset($post['filename'])) {
                $imgUrl = 'http://i.4cdn.org/' . $board . '/' . $post['tim'] . $post['ext'];
                $this->copyImg($imgUrl, Inflector::slug($post['no'] . '_' . $post['filename']) . $post['ext']);
            }
        }
    }
/**
 * Copies Img in Dir
 * @param  [type] $srcName    [description]
 * @param  [type] $targetName [description]
 * @return [type]             [description]
 */
    public function copyImg($srcName, $targetName) {
        copy($srcName, App::pluginPath('Gina') . 'webroot' . DS . 'img' . DS . $targetName);
        $this->out('Copy ' . $srcName . ' as ' . $targetName);
    }
}