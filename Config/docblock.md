#Gina Config
Examples for available data types

##primaryKey
returns the primary key from a related model

	array('type'=>'primaryKey','model'=>'Modelname');

##datetime
	array('type'=>'datetime', 'from'=>'2011-01-01', 'to'=>'2014-01-01', 'after'=>'created', 'precision' => 'hour', 'maxTimeSpan' => 10);

##name
	array('type'=>'name');

##firstname
	array('type'=>'firstname');

##lastname
	array('type'=>'lastname');

##company
	array('type'=>'company');

##job
	array('type'=>'job');

##text
	array('type'=>'text', 'paragraphs'=>2, 'book'=>'journey_in_other_worlds.txt');

##boolean
	array('type'=>'boolean');

##integer
	array('type'=>'integer', 'from'=> 100, 'to'=> 1000);
