<?php
App::uses('GinaAppModel', 'Gina.Model');
/**
 * Libterm Model
 *
 */
class Libterm extends GinaAppModel {

	public $useTable = 'gina';
	public $displayField = 'term';

	public $explicit = 0;

	public function getRandomname() {
		$randomWordQty = rand(2,3);
		$rndName = '';
		switch ($randomWordQty) {
			case 1: $grammatic[1] = 'substantive';
					break;
			case 2: $grammatic[1] = 'adjective';
			 		$grammatic[2] = 'substantive';
					break;
			case 3: $grammatic[1] = 'adjective';
					$grammatic[2] = 'adjective';
			 		$grammatic[3] = 'substantive';
					break;
		}
		for ($i=1;$i<=$randomWordQty; $i++) {
			$libtermQuery = $this->find('all', array(
				'conditions' => array(
					'grammatic' => $grammatic[$i],
					'explicit' => $this->explicit
				),
				'order' => 'rand()',
				'limit'=>1));
			$rndName .= $libtermQuery[0]['Libterm']['term'].' ';
		}
		$rndName = trim($rndName);
		return $rndName;
	}

	public function getJobtitle() {
		$grammatics[1] = 'job1';
		$grammatics[2] = 'job2';
 		$grammatics[3] = 'job3';
		$rndName = '';
		foreach ($grammatics as $key => $grammatic) {
			$libtermQuery = $this->find('all', array(
				'conditions' => array(
					'grammatic' => $grammatic,
					'explicit' => $this->explicit
				),
				'order' => 'rand()',
				'limit'=>1));
			$rndName .= $libtermQuery[0]['Libterm']['term'].' ';
		}
		$rndName = trim($rndName);
		return $rndName;
	}

	public function getBotname() {
		$randomWordQty = 2;
		$grammatic[1] = 'firstname';
		$grammatic[2] = 'lastname';
		for ($i=1;$i<=$randomWordQty; $i++) {
			$libtermQuery = $this->find('all', array(
				'conditions' => 'grammatic = "'.$grammatic[$i] .'"',
				'order' => 'rand()',
				'limit'=>1));
			$botName[$grammatic[$i]] = $libtermQuery[0]['Libterm']['term'];
		}
		$botName['username'] = strtolower($botName['firstname'].$botName['lastname']);
		return $botName;
	}

	public function getFirstname() {
		$libtermQuery = $this->find('all', array(
			'conditions' => array(
				'grammatic'  => 'firstname'
			),
			'order' => 'rand()',
			'limit'=>1));
		return $libtermQuery[0]['Libterm']['term'];
	}

	public function getLastname() {
		$libtermQuery = $this->find('all', array(
			'conditions' => array(
				'grammatic'  => 'lastname'
			),
			'order' => 'rand()',
			'limit'=>1));
		return $libtermQuery[0]['Libterm']['term'];
	}

	public function getAdvTeaser($actName = null) {
		$teaser = $actName.' are so ';
		$grammatic[1] = 'adjective';
		$libtermQuery = $this->find('all', array(
			'conditions' => 'grammatic = "'.$grammatic[1] .'"',
			'order' => 'rand()',
			'limit'=>1));
		$teaser .= $libtermQuery[0]['Libterm']['term'];
		$teaser .= '!';
		return $teaser;
	}

	public function getRandomtext($paragraphs = 2, $sampleData = "raven.txt") {
		$path = App::pluginPath('Gina');
		$textArray = file($path."Config" . DS . "SampleData" . DS . $sampleData);
		$rndText = '';
		$paragraphQty = count($textArray)-1;
		for ($i=0; $i<$paragraphs; $i++) {
			$rndText .= $textArray[rand(1, $paragraphQty)];
		}
		return $rndText;
	}

	public function getActname() {
 		return Inflector::pluralize($this->getRandomname());
	}

	public function getRandomWord($grammatic = 'adjective') {
		$libtermQuery = $this->find('all', array(
			'conditions' => array(
				'grammatic' => $grammatic,
				'explicit' => $this->explicit
			),
			'order' => 'rand()',
			'limit'=>1));
		return $libtermQuery[0]['Libterm']['term'];
	}

	public function getRandomDatetime($options = array()) {
		$defaults = array(
			'startTime' => null,
			'endTime' => null,
			'precision' => 'hour',
			'maxTimeSpan' => null
		);

		$options = array_merge($defaults, $options);

	    switch($options['precision']) {
	    	case 'min' :
	    		$timeScaler = 60;
	    		break;
	    	case 'hour' :
	    		$timeScaler = 3600;
	    		break;
    		case 'day' :
    			$timeScaler = 86400;
    			break;
	    	default : $timeScaler = 1;
	    }

	    $minSec = strtotime($options['startTime']);

	    if(isset($options['maxTimeSpan'])) {
	    	$maxSec = $minSec + ($options['maxTimeSpan'] * $timeScaler);
	    } else {
		    $maxSec = strtotime($options['endTime']);
	    }

	    $min = round($minSec / $timeScaler);
	    $max = round($maxSec / $timeScaler);
	    $val = rand($min, $max) * $timeScaler;

	    return date('Y-m-d H:i:s', $val);
	}

	public function getCompanyname() {
		$libtermQuery = $this->find('all', array(
			'conditions' => array(
				'grammatic' => 'company',
			),
			'order' => 'rand()',
			'limit'=>1));
		return $libtermQuery[0]['Libterm']['term'];
	}

}
