<?php

App::uses('Xml', 'Utility');

class MysqlXmlSource extends DataSource {

/**
 * An optional description of your datasource
 */
    public $description = 'A far away datasource';

/**
 * Our default config options. These options will be customized in our
 * ``app/Config/database.php`` and will be merged in the ``__construct()``.
 */
    public $config = array(

    );

/**
 * Create our HttpSocket and handle any config tweaks.
 */
    public function __construct($config) {
        parent::__construct($config);
        // $this->Http = new HttpSocket();
    }

/**
 * Since datasources normally connect to a database there are a few things
 * we must change to get them to work without a database.
 */

/**
 * listSources() is for caching. You'll likely want to implement caching in
 * your own way with a custom datasource. So just ``return null``.
 */
    public function listSources($data = null) {
        return null;
    }

/**
 * describe() tells the model your schema for ``Model::save()``.
 *
 * You may want a different schema for each model but still use a single
 * datasource. If this is your case then set a ``schema`` property on your
 * models and simply return ``$model->schema`` here instead.
 */
    public function describe($model) {
        return $this->_schema;
    }

/**
 * calculate() is for determining how we will count the records and is
 * required to get ``update()`` and ``delete()`` to work.
 *
 * We don't count the records here but return a string to be passed to
 * ``read()`` which will do the actual counting. The easiest way is to just
 * return the string 'COUNT' and check for it in ``read()`` where
 * ``$data['fields'] === 'COUNT'``.
 */
    public function calculate(Model $model, $func, $params = array()) {
        return 'COUNT';
    }

/**
 * Implement the R in CRUD. Calls to ``Model::find()`` arrive here.
 */
    public function read(Model $model, $queryData = array(),
        $recursive = null) {
        /**
         * Here we do the actual count as instructed by our calculate()
         * method above. We could either check the remote source or some
         * other way to get the record count. Here we'll simply return 1 so
         * ``update()`` and ``delete()`` will assume the record exists.
         */
        if ($queryData['fields'] === 'COUNT') {
            return array(array(array('count' => 1)));
        }
        /**
         * Now we get, decode and return the remote data.
         */
        // $queryData['conditions']['apiKey'] = $this->config['apiKey'];
        // $json = $this->Http->get(
        //     'http://example.com/api/list.json',
        //     $queryData['conditions']
        // );
        // $res = json_decode($json, true);
        // if (is_null($res)) {
        //     $error = json_last_error();
        //     throw new CakeException($error);
        // }

        $path = App::pluginPath('Gina');
        // $xml = Xml::build($path . "Config" . DS . "SampleData" . DS . 'data.xml');
        $xmlString = $path . "Config" . DS . "SampleData" . DS . 'data.xml';
        // $xmlObject = new Xml($path . "Config" . DS . "SampleData" . DS . 'data.xml');
        // $xmlArray = $xmlObject->toArray();

        $xmlObject = Xml::build($xmlString, array('return' => 'simplexml'));
        $xmlArray = Xml::toArray($xmlObject->database->table_data);

        debug($xmlObject->database->table_data);
        // debug($xmlArray);
        // foreach ($xmlArray['Libterm']['mysqldump'] as $key => $value) {
        //     debug($key);
        // }
die();
        // $res = $xmlArray;
        // return array($model->alias => $res);
    }

}
